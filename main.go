package main

import (
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/lrstanley/girc"
	"github.com/mmcdole/gofeed"
	"github.com/spf13/viper"
)

type News struct {
	Origin string `json:"origin"`
	Title  string `json:"title"`
	Link   string `json:"link"`
	Date   string `json:"date"`
	Hash   string `json:"hash"`
}

var newsList []News
var pendingList []News

// Send an IRC ircMessage, unless in dry-run mode
func ircMessage(client *girc.Client, target, message string) {
	if viper.GetBool("irc.dry") {
		log.Printf("ircMessage to %s: %s", target, message)
	} else {
		client.Cmd.Message(target, message)
	}
}

// Create a hash out of link
func mkHash(s string) string {
	h := sha256.Sum256([]byte(s))
	hash := hex.EncodeToString(h[:])
	return hash[:8]
}

// Checks if news exists by searching its hash
func newsExists(news News) bool {
	for _, n := range newsList {
		if n.Hash == news.Hash {
			return true
		}
	}
	return false
}

func saveNews(news News) {
	if len(newsList) < viper.GetInt("feeds.ringsize") {
		newsList = append(newsList, news)
	} else {
		newsList = append(newsList[1:], news)
	}
}

// Retrieve a news by its hash
func getNewsByHash(hash string) News {
	hash = strings.ReplaceAll(hash, "#", "")
	for _, n := range newsList {
		if n.Hash == hash {
			return n
		}
	}
	return News{}
}

// Retrieve news from a certain origin
func getNewsByOrigin(origin string) []News {
	resNews := []News{}

	for _, n := range newsList {
		if n.Origin == origin {
			resNews = append(resNews, n)
		}
	}
	return resNews
}

func fmtNews(news News) string {
	colorReset := girc.Fmt("{r}")
	colorOrigin := girc.Fmt(fmt.Sprintf("{%s}", viper.GetString("irc.colors.origin")))
	colorTitle := girc.Fmt(fmt.Sprintf("{%s}", viper.GetString("irc.colors.title")))
	colorLink := girc.Fmt(fmt.Sprintf("{%s}", viper.GetString("irc.colors.link")))
	colorHash := girc.Fmt(fmt.Sprintf("{%s}", viper.GetString("irc.colors.hash")))

	return fmt.Sprintf("[%s%s%s] %s%s%s %s%s%s %s#%s%s",
		colorOrigin, news.Origin, colorReset,
		colorTitle, news.Title, colorReset,
		colorLink, news.Link, colorReset,
		colorHash, news.Hash, colorReset)
}

// Fetch and post news from RSS feeds
func newsFetch(client *girc.Client, channel string) {

	newsList = make([]News, 0)

	feedFile := channel + "-feed.json"
	// load saved news
	f, err := os.OpenFile(feedFile, os.O_CREATE|os.O_RDWR, 0o644)
	if err != nil {
		log.Fatalf("can't open %s: %v", feedFile, err)
	}
	defer f.Close()
	decoder := json.NewDecoder(f)
	if err := decoder.Decode(&newsList); err != nil {
		log.Println("could not load news list, empty?")
	}

	for {
		// Only check chanlist if target is not a query
		if client.IsConnected() && (!strings.HasPrefix(channel, "#") || len(client.ChannelList()) != 0) {
			break
		}
		log.Printf("%v, not connected, waiting...\n", client.ChannelList())

		time.Sleep(viper.GetDuration("feeds.frequency"))
	}

	for {

		if viper.GetBool("irc.secondary") {
			// Post news from previous round if still fresh
			for _, news := range pendingList {
				if newsExists(news) {
					log.Printf("already caught-up %s (%s)\n", news.Title, news.Hash)
					continue
				}

				ircMessage(client, channel, fmtNews(news))
				time.Sleep(viper.GetDuration("irc.delay"))
				saveNews(news)
			}
			pendingList = pendingList[:0]
		}

		for _, feedURL := range viper.GetStringSlice("feeds.urls") {
			log.Printf("fetching %s...\n", feedURL)
			fp := gofeed.NewParser()
			feed, err := fp.ParseURL(feedURL)
			if err != nil {
				log.Printf("Failed to fetch feed '%s': %s", feedURL, err)
				continue
			}

			i := 0 // number of posted news
			for _, item := range feed.Items {
				if item.PublishedParsed == nil {
					log.Printf("Invalid Published Parsed in %q / %q (from %q)\n", feed.Title, item.Title, item.Published)
					continue
				}
				news := News{
					Origin: feed.Title,
					Title:  item.Title,
					Link:   item.Link,
					Date:   item.PublishedParsed.String(),
					Hash:   mkHash(item.Link),
				}
				// Check if item was already posted
				if newsExists(news) {
					log.Printf("already posted %s (%s)\n", item.Title, news.Hash)
					continue
				}
				// don't paste news older than feeds.maxage
				if time.Since(*item.PublishedParsed) > viper.GetDuration("feeds.maxage") {
					log.Printf("news too old (%s)\n", item.Published)
					continue
				}
				i++
				if i > viper.GetInt("feeds.maxnews") {
					log.Println("too many lines to post")
					break
				}

				if viper.GetBool("irc.secondary") {
					// Hold the news for one whole cycle
					pendingList = append(pendingList, news)
					continue
				}

				ircMessage(client, channel, fmtNews(news))
				time.Sleep(viper.GetDuration("irc.delay"))
				saveNews(news)
			}
		}
		// save news list to disk to avoid repost when restarting
		if err := f.Truncate(0); err != nil {
			log.Fatal(err)
		}
		if _, err = f.Seek(0, 0); err != nil {
			log.Fatal(err)
		}
		encoder := json.NewEncoder(f)
		if err = encoder.Encode(newsList); err != nil {
			ircMessage(client, channel, "could not write newsList")
		}
		time.Sleep(viper.GetDuration("feeds.frequency"))
	}
}

func confDefault() {
	kv := map[string]interface{}{
		"irc.server":        "irc.libera.chat",
		"irc.nick":          "gruik",
		"irc.channel":       "goaste",
		"irc.sasl":          true,
		"irc.xchannels":     []string{"goaste2"},
		"irc.debug":         false,
		"irc.dry":           false,
		"irc.secondary":     false,
		"irc.port":          6667,
		"irc.delay":         "2s",
		"irc.colors.origin": "pink",
		"irc.colors.title":  "bold",
		"irc.colors.link":   "lightblue",
		"irc.colors.hash":   "lightgrey",
		"feeds.urls":        []string{},
		"feeds.maxnews":     10,
		"feeds.maxage":      "1h",
		"feeds.frequency":   "10m",
		"feeds.ringsize":    100,
	}

	for k, v := range kv {
		if !viper.IsSet(k) {
			viper.Set(k, v)
		}
	}
}

func isOp(nick string) bool {
	for _, op := range viper.GetStringSlice("irc.ops") {
		if nick == op {
			return true
		}
	}
	return false
}

// Get the second part of a command
func getParam(s string) string {
	if !strings.Contains(s, " ") {
		return ""
	}
	return s[strings.LastIndex(s, " ")+1:]
}

func main() {
	config := "config"
	if len(os.Args) > 1 {
		config = os.Args[1]
	}

	viper.SetConfigName(config)
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")
	viper.WatchConfig()

	if err := viper.ReadInConfig(); err != nil {
		log.Fatalf("Failed to read configuration file: %s", err)
	}

	nick := viper.GetString("irc.nick")
	password := viper.GetString("irc.password")
	name := nick
	user := strings.ToLower(nick)
	channel := viper.GetString("irc.channel")
	debug := io.Discard
	if viper.GetBool("irc.debug") {
		debug = os.Stdout
	}

	confDefault() // load defaults for unset parameters

	client := girc.New(girc.Config{
		Server:     viper.GetString("irc.server"),
		Nick:       nick,
		Port:       viper.GetInt("irc.port"),
		Debug:      debug,
		User:       user,
		Name:       name,
		AllowFlood: true,
	})
	if len(password) > 0 {
		if viper.GetBool("irc.sasl") == true {
			client.Config.SASL = &girc.SASLPlain{
				User: user,
				Pass: password,
			}
		} else {
			client.Config.ServerPass = password
		}

	}

	client.Handlers.Add(girc.CONNECTED, func(c *girc.Client, e girc.Event) {
		if strings.HasPrefix(channel, "#") {
			c.Cmd.Join(channel)
		}

		// join secondary channels for xposting
		for _, xchan := range viper.GetStringSlice("irc.xchannels") {
			if strings.HasPrefix(xchan, "#") {
				c.Cmd.Join(xchan)
			}
		}
	})
	client.Handlers.Add(girc.PRIVMSG, func(c *girc.Client, e girc.Event) {
		dest := channel

		if len(e.Params) > 0 && e.Params[0] != channel {
			dest = e.Source.Name
		}

		if viper.GetBool("irc.secondary") {
			for _, suffix, found := strings.Cut(e.Last(), "#"); found && len(suffix) >= 8; _, suffix, found = strings.Cut(suffix, "#") {
				if strings.Trim(suffix[:8], "0123456789abcdef") == "" {
					log.Printf("Received hash %s from %s", suffix[:8], e.Source.Name)
					news := News{Hash: suffix[:8]}
					if !newsExists(news) {
						saveNews(news)
					}
				}
			}
		}

		if strings.HasPrefix(e.Last(), "!lsfeeds") {
			for i, f := range viper.GetStringSlice("feeds.urls") {
				n := strconv.Itoa(i + 1)
				ircMessage(c, dest, n+". "+f)
				time.Sleep(viper.GetDuration("irc.delay"))
			}
		}
		if strings.HasPrefix(e.Last(), "!xpost") && e.Params[0] == channel && !viper.GetBool("irc.secondary") {
			requestedHash := getParam(e.Last())
			if requestedHash == "" {
				return
			}
			if news := getNewsByHash(requestedHash); news.Hash != "" {
				post := fmt.Sprintf(" {r}(from %s on %s)", e.Source.Name, channel)
				message := fmtNews(news) + girc.Fmt(post)
				for _, xchan := range viper.GetStringSlice("irc.xchannels") {
					ircMessage(c, xchan, message)
					time.Sleep(viper.GetDuration("irc.delay"))
				}
			}
		}
		if strings.HasPrefix(e.Last(), "!latest") && e.Params[0] != channel {
			args := strings.SplitN(e.Last(), " ", 3)
			if len(args) < 2 {
				ircMessage(c, dest, "usage: !latest <number> [origin]")
				time.Sleep(viper.GetDuration("irc.delay"))
				return
			}

			// n == number of news to show
			n, err := strconv.Atoi(args[1])
			if err != nil || n <= 0 {
				ircMessage(c, dest, "conversion error")
				time.Sleep(viper.GetDuration("irc.delay"))
				return
			}

			// default to all news
			showNews := newsList
			numNews := len(showNews)

			// there was a second parameter, specific origin
			if len(args) > 2 {
				showNews = getNewsByOrigin(args[2])
				numNews = len(showNews)
			}

			// check if some news are available
			if numNews < 1 {
				ircMessage(c, dest, "no news available")
				time.Sleep(viper.GetDuration("irc.delay"))
				return
			}

			// user gave a greater number that we have news
			if n > numNews {
				n = numNews
			}
			numNews--
			for i := 0; i < n; i++ {
				fmt.Println(i)
				ircMessage(c, dest, fmtNews(showNews[numNews-i]))
				time.Sleep(viper.GetDuration("irc.delay"))
			}
		}

		// All commands below requires OP
		if !isOp(e.Source.Name) {
			return
		}

		if strings.HasPrefix(e.Last(), "!die") {
			c.Close()
		}
		if strings.HasPrefix(e.Last(), "!addfeed") {
			url := getParam(e.Last())
			if url == "" {
				return
			}
			feeds := viper.GetStringSlice("feeds.urls")
			for _, feed := range feeds {
				if feed == url {
					return
				}
			}
			feeds = append(feeds, url)
			viper.Set("feeds.urls", feeds)
			c.Cmd.ReplyTo(e, girc.Fmt("feed {b}{green}added{c}{b}"))
			if err := viper.WriteConfig(); err != nil {
				c.Cmd.ReplyTo(e, girc.Fmt("adding feed {b}{red}failed{c}{b}"))
			}
		}
		if strings.HasPrefix(e.Last(), "!rmfeed") {
			feeds := viper.GetStringSlice("feeds.urls")
			s := getParam(e.Last())
			if s == "" {
				return
			}
			i, err := strconv.Atoi(s)
			if err != nil {
				c.Cmd.ReplyTo(e, "index conversion failed")
				return
			}
			if i < 1 || i > len(feeds) {
				c.Cmd.ReplyTo(e, "bad index number")
				return
			}
			feeds = append(feeds[:i-1], feeds[i:]...)
			viper.Set("feeds.urls", feeds)
			c.Cmd.ReplyTo(e, girc.Fmt("feed {b}{green}removed{c}{b}"))
			if err := viper.WriteConfig(); err != nil {
				c.Cmd.ReplyTo(e, girc.Fmt("removing feed {b}{red}failed{c}{b}"))
			}
		}
	})

	go newsFetch(client, channel)

	for {
		if err := client.Connect(); err != nil {
			log.Printf("Failed to connect to IRC server: %s", err)
			log.Println("reconnecting in 30 seconds...")
			time.Sleep(30 * time.Second)
		} else {
			return
		}
	}
}
